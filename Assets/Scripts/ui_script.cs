using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class ui_script : MonoBehaviour
{
    private float time_alive = 0;
    // Start is called before the first frame update
    public GameObject sprite;
    void Start()
    {
        StartCoroutine(MyCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TextMeshProUGUI>().SetText("Puntuaci�: "+this.time_alive);
    }
    IEnumerator MyCoroutine()
    {
        while (!this.sprite.IsDestroyed())
        {
            yield return new WaitForSeconds(1.0f);
            this.time_alive++;

        }
    }
}
