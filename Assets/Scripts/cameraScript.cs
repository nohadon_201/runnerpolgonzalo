using System;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class cameraScript : MonoBehaviour
{
    public GameObject[] u;
    public GameObject[] d;
    public GameObject[] utd;
    public GameObject[] dtu;
    private GameObject newBlock;
    public GameObject sprite;
    private float puntero = -10;
    private String tag_block;
    // Start is called before the first frame update
    void Start()
    {
        {
            this.tag_block= "d";
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Console.Write(this.transform.position.x);
        if (!sprite.IsDestroyed())
        {
            transform.position = new Vector3(this.sprite.transform.position.x, this.transform.position.y, this.sprite.transform.position.z - 100);
        }
        if (this.transform.position.x >= this.puntero)
        {
            if (puntero == -10)
            {
                this.puntero = this.puntero + 10.5f;
            }
            else
            {
                this.puntero = this.puntero + 21f;
            }
            
            if (this.tag_block == "container_dtu")
            {
                this.newBlock = Instantiate(dtu[Random.Range(0, dtu.Length)]);
            }else if (this.tag_block == "container_utd")
            {
                this.newBlock = Instantiate(utd[Random.Range(0, utd.Length)]);
            }
            else if (this.tag_block == "container_u")
            {
                this.newBlock = Instantiate(u[Random.Range(0, u.Length)]);
               
            }
            else
            {
                this.newBlock = Instantiate(d[Random.Range(0, d.Length)]);
            }
            newBlock.transform.position = new Vector2(puntero, 0);
            this.tag_block = newBlock.tag;
            
        }
        
    }
}
