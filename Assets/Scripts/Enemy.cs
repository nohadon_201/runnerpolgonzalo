using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    private Vector2 up;
    private Vector2 down;
    // Start is called before the first frame update
    void Start()
    {
        this.up = new Vector2(0, 3);
        this.down = new Vector2(0, -3);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.tag == "enemy_du")
        {
            if (this.transform.position.y >= -0.34f)
            {
                this.GetComponent<Rigidbody2D>().velocity = this.down;
            }else if(this.transform.position.y <= -3.16f)
            {
                this.GetComponent<Rigidbody2D>().velocity = this.up;
            }
            
        }
        else if (this.tag == "enemy_ud")
        {
            if (this.transform.position.y >= 4f)
            {
                this.GetComponent<Rigidbody2D>().velocity = this.down;
            }
            else if (this.transform.position.y <= 1.26f)
            {
                this.GetComponent<Rigidbody2D>().velocity = this.up;
            }

        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Sprite_prin")
        {
            Destroy(collision.gameObject);
            SceneManager.LoadScene("Died");
        }
    }
}
