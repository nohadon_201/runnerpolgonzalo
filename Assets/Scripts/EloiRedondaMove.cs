using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EloiRedondaMove : MonoBehaviour
{
    public Rigidbody2D rb;
    private Boolean jump;
    private bool dash=false;
    private float time;
    private float vel;
    private float time_max;
    // Start is called before the first frame update
    void Start()
    {
        time_max = 10;
        vel = 4;
        this.time = 0;
        this.rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        this.time += Time.deltaTime;
        if(this.time >= time_max)
        {
            time_max += 10;
            this.vel += 0.1f;
        }
        rb.velocity=new Vector2 (vel,rb.velocity.y);
        if(Input.GetKeyDown(KeyCode.Space) && !this.jump)
        {

            this.jump = true;
            if (this.rb.gravityScale < 0)
            {
                rb.AddForce(new Vector2(this.rb.velocity.x, -500));
            }
            else
            {
                rb.AddForce(new Vector2(this.rb.velocity.x, 500));
            }
            
        }else if(Input.GetKeyDown(KeyCode.Space) && !this.dash)
        {
            this.dash=true;
            rb.AddForce(new Vector2(5000,0));
        }
        if (this.transform.position.y > 7 || this.transform.position.y < -8)
        {
            this.morirseDelAsco();
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "suelo")
        {
            this.jump = false;
            this.dash=false;
        }
    }
    public void morirseDelAsco()
    {
        Destroy(this);
        SceneManager.LoadScene("Died");
    }
}
