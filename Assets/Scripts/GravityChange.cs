using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Sprite_prin")
        {
            Rigidbody2D er = collision.gameObject.GetComponent<Rigidbody2D>();
            if (er.gravityScale > 0)
            {
                er.gravityScale = -2;
            }
            else
            {
                er.gravityScale = 2;
            }
            
        }
    }
}
